

plugins {
    id("org.springframework.boot") version "2.5.3"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.5.21"
    kotlin("plugin.spring") version "1.5.21"
    kotlin("plugin.jpa") version "1.5.21"
    id("com.bmuschko.docker-spring-boot-application") version "7.1.0"
    id("com.bmuschko.docker-remote-api") version "7.1.0"
    id("com.gorylenko.gradle-git-properties") version "2.3.1"
    id("org.barfuin.gradle.taskinfo") version "1.3.0"
}

docker {
    springBootApplication {
        baseImage.set("openjdk:11-jre-slim")
        images.set(setOf("registry.gitlab.com/kinjalik/financial-tracker:" + (System.getenv("CI_COMMIT_SHA") ?: "local")))
    }



    registryCredentials {
        url.set(System.getenv("CI_REGISTRY") ?: "http://registry.gitlab.com")
        username.set(System.getenv("CI_REGISTRY_USER"))
        password.set(System.getenv("CI_REGISTRY_PASSWORD"))
        email.set("albert@kinjalik.ru")
    }

}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

repositories {
    mavenCentral()
    google()
}

tasks {
    test {
        useJUnitPlatform()
    }

}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("com.google.code.gson:gson")

    implementation("org.testcontainers:testcontainers:1.16.0")
    implementation("org.testcontainers:postgresql:1.16.0")
    implementation("org.postgresql:postgresql")
    implementation("org.springdoc:springdoc-openapi-ui:1.5.10")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.liquibase:liquibase-core:4.4.3")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    implementation("com.google.api-client:google-api-client:1.32.1")
}
