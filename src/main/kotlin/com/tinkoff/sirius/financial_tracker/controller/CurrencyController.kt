package com.tinkoff.sirius.financial_tracker.controller

import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.tinkoff.sirius.financial_tracker.dto.CurrencyDto
import com.tinkoff.sirius.financial_tracker.services.CurrencyService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*
import java.net.URL
import javax.transaction.Transactional

@RestController
@RequestMapping("currencies")
@Tag(name="Валюты")
class CurrencyController(
    private val currencyService: CurrencyService
) {

    @GetMapping("")
    @Operation(summary = "Получить список всех валют")
    fun getCurrencies(): List<CurrencyDto> =
        currencyService.getAllCurrencies()

    @Transactional
    @GetMapping("/{input}_{output}")
    @Operation(summary = "Конвертация валют")
    fun getConvertValue(
        @PathVariable("input") input : String,
        @PathVariable("output") output : String
    ) : Double {
       return currencyService.getConvertValue(input, output, true)
    }
}