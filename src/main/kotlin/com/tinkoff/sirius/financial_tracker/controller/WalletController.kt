package com.tinkoff.sirius.financial_tracker.controller

import com.tinkoff.sirius.financial_tracker.dto.*
import com.tinkoff.sirius.financial_tracker.entities.Transaction
import com.tinkoff.sirius.financial_tracker.entities.Wallet
import com.tinkoff.sirius.financial_tracker.services.TransactionService
import com.tinkoff.sirius.financial_tracker.services.UserService
import com.tinkoff.sirius.financial_tracker.services.WalletService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.tags.Tag
import io.swagger.v3.oas.annotations.parameters.RequestBody as SwaggerRequestBody
import org.springframework.web.bind.annotation.*
import javax.transaction.Transactional

@RestController
@RequestMapping("wallets")
@Tag(name = "Кошельки")
class WalletController(
    private val walletService: WalletService,
    private val transactionService: TransactionService
) {

    @GetMapping("/{id}")
    @Operation(summary = "Получить кошелек по его ID")
    fun getWallet(@PathVariable id: Int, @RequestHeader("X-Email") email: String): WalletDto? {
        return walletService.findWalletById(id)
    }

    @PostMapping
    @Operation(
        summary = "Добавить кошелек", requestBody =
        SwaggerRequestBody(
            content = [Content(
                examples = [ExampleObject(
                    name = "Успешный вызов",
                    value = """
            {
  "name": "Test wallet",
  "currency": {
    "shortName": "RUB"
  },
  "balanceLimit": 10000
}
        """
                )]
            )]
        )
    )
    fun addWallet(@RequestBody wallet: WalletDto, @RequestHeader("X-Email", defaultValue = "test@test-mail.com") email: String): WalletDto? {
        return walletService.addWallet(wallet, email)
    }

    @GetMapping("/{id}/income")
    @Operation(summary = "Получить сумму доходов в кошельке по ID")
    fun getIncomeIntegerPart(@PathVariable id: Int): NumberResultDto {
        return transactionService.incomeTransactionAmountByWalletId(id)
    }

    @GetMapping("/{id}/expenditure")
    @Operation(summary = "Получить сумму расходов в кошельке по ID")
    fun getExpenditureIntegerPart(@PathVariable id: Int): NumberResultDto {
        return transactionService.expenditureTransactionAmountByWalletId(id)
    }

    @GetMapping("/{id}/sum")
    @Operation(summary = "Получить общий баланс кошелька по ID")
    fun sumTransactionsAmount(@PathVariable id: Int): NumberResultDto {
        return transactionService.sumTransactionAmountByWalletId(id)
    }

    @GetMapping
    @Operation(summary = "Получить кошельки пользователя")
    fun getWallets(@RequestHeader("X-Email") userEmail: String): List<WalletDto> {
        return walletService.findWalletsByUserEmail(userEmail)
    }

    @Transactional
    @GetMapping("/stat")
    @Operation(summary = "Получить информацию о всех кошельках пользователя")
    fun getWalletsStat(@RequestHeader("X-Email") userEmail: String): WalletStatDto {
        val currency = CurrencyDto("RUB", 2)
        return walletService.getWalletStatDto(userEmail, currency)
    }

    @GetMapping("/{id}/transactions")
    @Operation(summary = "Получить тразакции в кошельке по ID")
    fun getTransactionsByWalletId(
        @PathVariable id: Int,
        @RequestParam("lastId", required = false) lastId: Int?,
        @RequestParam("limit", defaultValue = "10") limit: Int
    ): List<TransactionDto> {
        return transactionService.getTransactionsByWalletId(id, lastId, limit)
    }

    @Transactional
    @GetMapping("{id}/hide")
    @Operation(summary = "Скрыть кошелек")
    fun hideWallet(@PathVariable id: Int) =
        walletService.hideWallet(id)

    @Transactional
    @GetMapping("{id}/show")
    @Operation(summary = "Показать кошелек")
    fun showWallet(@PathVariable id: Int) =
        walletService.showWallet(id)

    @Transactional
    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить кошелек")
    fun deleteWallet(@PathVariable id: Int,
                     @RequestHeader("X-Email", defaultValue = "test@test-mail.com") email: String) {
        walletService.deleteWalletById(id)
    }

    @Transactional
    @PatchMapping("/{id}")
    @Operation(summary = "Name and balanceLimit change here")
    fun updateWallet(
        @PathVariable id: Int,
        @RequestHeader("X-Email", defaultValue = "test@test-mail.com") email: String,
        @RequestBody wallet: WalletDto
    ): WalletDto {
        return walletService.updateWalletById(id, wallet)
    }



}
