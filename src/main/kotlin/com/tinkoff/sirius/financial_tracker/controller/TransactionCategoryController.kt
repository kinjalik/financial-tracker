package com.tinkoff.sirius.financial_tracker.controller

import com.tinkoff.sirius.financial_tracker.entities.TransactionCategory
import com.tinkoff.sirius.financial_tracker.services.TransactionCategoryService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*
import io.swagger.v3.oas.annotations.parameters.RequestBody as SwaggerRequestBody

@RestController
@RequestMapping("categories")
@Tag(name = "Категории транзакций")
class TransactionCategoryController(
    private val transactionCategoryService: TransactionCategoryService
) {
    @GetMapping
    @Operation(
        summary = "Получить список категорий пользователя"
    )
    fun getCategoriesForUser(
        @RequestHeader("X-Email") email: String
    ): List<TransactionCategory> {
        // ToDo: make separation of categories by user's email
        return transactionCategoryService.getTxCategoriesForUserByEmail(email)
    }

    @PostMapping
    @Operation(
        summary = "Добавить категорию для пользователя",
        requestBody = SwaggerRequestBody(
            content = [Content(
                examples = [ExampleObject(
                    name = "Добавление доходной категории",
                    value = """
                    {
  "name": "Галера",
  "iconColor": 538395,
  "iconLink": "icons/8.svg",
  "income": true
}
                """
                ), ExampleObject(
                    name = "Добавление расходной категории",
                    value = """
                        {
                        "name": "Презервативы",
                        "iconColor": 24242,
                        "iconLink": "icons/3.svg",
                        "income": false
                        }
                    """
                )]
            )]
        )
    )
    fun addCategory(
        @RequestHeader("X-Email") email: String,
        @RequestBody txCat: TransactionCategory
    ): TransactionCategory {
        return transactionCategoryService.createIfNotExists(txCat, email)
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление категории транзакций")
    fun deleteCategory(
        @RequestHeader("X-Email") email: String,
        @PathVariable("id") id: Int
    ) {
        return transactionCategoryService.deleteCategory(email, id)
    }
}