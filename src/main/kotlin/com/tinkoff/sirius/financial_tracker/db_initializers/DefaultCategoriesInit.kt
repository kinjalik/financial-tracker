package com.tinkoff.sirius.financial_tracker.db_initializers

import com.tinkoff.sirius.financial_tracker.entities.TransactionCategory
import com.tinkoff.sirius.financial_tracker.services.TransactionCategoryService
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import kotlin.random.Random

@Configuration
class DefaultCategoriesInit {
    @Bean
    fun categoryDatabaseInitializer(transactionCategoryService: TransactionCategoryService)
    = ApplicationRunner {
        val defaultCategories = listOf(
            // TODO добавить ссылки и цвета, инициализация для каждого нового пользователя
            TransactionCategory(name="Зарплата", isIncome = true, iconLink = "19"),
            TransactionCategory(name="Подработка", isIncome = true, iconLink = "9"),
            TransactionCategory(name="Подарок", isIncome = true, iconLink = "27"),
            TransactionCategory(name="Капитализация", isIncome = true, iconLink = "10"),

            TransactionCategory(name="Кафе и рестораны", isIncome = false, iconLink = "15"),
            TransactionCategory(name="Супермаркеты", isIncome = false, iconLink = "0"),
            TransactionCategory(name="Спортзал", isIncome = false, iconLink = "21"),
            TransactionCategory(name="Общественный транспорт", isIncome = false, iconLink = "11"),
            TransactionCategory(name="Медицина", isIncome = false, iconLink = "23"),
            TransactionCategory(name="Бензин", isIncome = false, iconLink = "18"),
            TransactionCategory(name="Кварплата", isIncome = false, iconLink = "29"),
            TransactionCategory(name="Отпуск", isIncome = false, iconLink = "7"),
        )

        val rand = Random(2842928)
        defaultCategories
            .map { it.apply {
                iconLink = "icons/$iconLink.svg"
                iconColor = rand.nextInt()
                userId = null
            } }
            .forEach(transactionCategoryService::createIfNotExists)
    }
}