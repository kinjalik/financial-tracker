package com.tinkoff.sirius.financial_tracker.db_initializers

import com.tinkoff.sirius.financial_tracker.entities.Currency
import com.tinkoff.sirius.financial_tracker.services.CurrencyService
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CurrenciesInit {
    @Bean
    fun currencyDatabaseInitializer(currencyService: CurrencyService) = ApplicationRunner {
        currencyService.createIfNotExists(
            Currency(shortName = "RUB", decimalDigits = 2)
        )
        currencyService.createIfNotExists(
            Currency(shortName = "USD", decimalDigits = 2)
        )
        currencyService.createIfNotExists(
            Currency(shortName = "EUR", decimalDigits = 2)
        )
        currencyService.createIfNotExists(
            Currency(shortName = "BTC", decimalDigits = 18)
        )
    }
}