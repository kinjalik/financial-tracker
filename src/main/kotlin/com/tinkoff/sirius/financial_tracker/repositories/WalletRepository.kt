package com.tinkoff.sirius.financial_tracker.repositories

import com.tinkoff.sirius.financial_tracker.entities.Wallet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WalletRepository : JpaRepository<Wallet, Int> {
    fun findWalletById(id : Int) : Wallet
    fun findWalletsByUserId(userId : Int) : List<Wallet>
    fun save(wallet : Wallet) : Wallet
    fun deleteWalletById(id: Int)
}