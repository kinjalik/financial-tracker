package com.tinkoff.sirius.financial_tracker.repositories

import com.tinkoff.sirius.financial_tracker.entities.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository: CrudRepository<User, Int> {
    fun findUserByEmail(email: String): User?

    fun findUserById(id: Int): User?
}