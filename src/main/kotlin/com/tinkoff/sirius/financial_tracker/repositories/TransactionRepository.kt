package com.tinkoff.sirius.financial_tracker.repositories

import com.tinkoff.sirius.financial_tracker.entities.Transaction
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDate
import java.time.LocalDateTime

@Repository
interface TransactionRepository : CrudRepository<Transaction, Int> {
    fun findTransactionById(id : Int) : Transaction?

    fun findTransactionsByWalletId(walletId: Int): List<Transaction>

    fun deleteTransactionById(id : Int)

    fun deleteTransactionsByWalletId(walletId: Int)

//    @Query("SELECT COALESCE(SUM(amount), 0) FROM Transaction where (walletId in (SELECT Wallet.id FROM Wallet WHERE userId = userId)) AND amount > 0",
//                                                                                                    nativeQuery = true)
//    fun sumTransactionAmountByUserId(@Param("userId") useId : Int) : Int
//
//    @Query("SELECT COALESCE(SUM(amount), 0) FROM Transaction where (walletId in (SELECT Wallet.id FROM Wallet WHERE userId = userId)) AND amount > 0",
//                                                                                                    nativeQuery = true)
//
//    fun incomeTransactionAmountByUserId(@Param("userId") userId : Int) : Int
//
//    @Query("SELECT COALESCE(-SUM(amount), 0) FROM Transaction where (walletId in (SELECT Wallet.id FROM Wallet WHERE userId = userId)) AND amount < 0",
//                                                                                                    nativeQuery = true)
//    fun expenditureTransactionAmountByUserId(@Param("userId") userId : Int) : Int

    @Query("SELECT COALESCE(SUM(amount), 0) FROM Transaction where walletId = :walletId")
    fun sumTransactionAmountByWalletId(@Param("walletId") walletId : Int) : Int

    @Query("SELECT COALESCE(SUM(amount), 0) FROM Transaction WHERE walletId = :walletId AND amount > 0")
    fun incomeTransactionAmountByWalletId(@Param("walletId") walletId : Int) : Int

    @Query("SELECT COALESCE(-SUM(amount), 0) FROM Transaction where walletId = :walletId and amount < 0")
    fun expenditureTransactionAmountByWalletId(@Param("walletId") walletId : Int) : Int

    @Query("SELECT * FROM transaction WHERE wallet_id = :walletId ORDER BY date DESC LIMIT :limit", nativeQuery = true)
    fun getFirstTransactionPage(@Param("walletId") walletId: Int, @Param("limit") limit: Int): List<Transaction>

    @Query("SELECT * FROM (" +
            "SELECT * FROM transaction WHERE wallet_id = :walletId AND date < :date ORDER BY date DESC" +
            ") AS t LIMIT :limit", nativeQuery = true)
    fun getTransactionPage(
        @Param("walletId") walletId: Int,
        @Param("date") date: LocalDateTime,
        @Param("limit") limit: Int
    ): List<Transaction>
}