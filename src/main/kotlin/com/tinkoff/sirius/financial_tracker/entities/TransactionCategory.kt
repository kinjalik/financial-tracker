package com.tinkoff.sirius.financial_tracker.entities

import io.swagger.v3.oas.annotations.media.Schema
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class TransactionCategory (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = 0,
    var name: String,
    var isIncome: Boolean,
    var iconColor: Int? = null,
    var iconLink: String? = null,
    var userId: Int? = null
)
