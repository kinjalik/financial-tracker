package com.tinkoff.sirius.financial_tracker.entities

import javax.persistence.*

@Entity
data class Wallet(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = 0,
    var name: String,
    var userId: Int? = null,
    @OneToOne
    var currency: Currency,
    var balanceLimit: Int? = null,
    var isHidden: Boolean = false
)
