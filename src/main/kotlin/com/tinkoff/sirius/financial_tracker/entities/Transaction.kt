package com.tinkoff.sirius.financial_tracker.entities

import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Transaction(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,
    @Column(name = "wallet_id")
    var walletId: Int,
    @Column(name = "category_id")
    var categoryId: Int,

    @Column(name = "amount")
    var amount: Int,
//    @CreationTimestamp
    @Column(name = "date")
    var date: LocalDateTime
)
