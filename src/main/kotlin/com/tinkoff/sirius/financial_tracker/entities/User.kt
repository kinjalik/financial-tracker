package com.tinkoff.sirius.financial_tracker.entities

import javax.persistence.*

@Entity(name="users")
data class User (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = 0,
    var email: String? = null
)