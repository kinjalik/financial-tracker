package com.tinkoff.sirius.financial_tracker.configuration

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.PathItem
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.links.Link
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SwaggerConfiguration {
    @Bean
    fun swagger(): OpenAPI = OpenAPI()
        .info(Info()
            .title("Financial Tracker API")
            .description("Документация по API"))
        .components(Components()
            .addSecuritySchemes("Google Auth Token", SecurityScheme()
                .type(SecurityScheme.Type.APIKEY)
                .`in`(SecurityScheme.In.HEADER)
                .name("X-Token")))
        .addSecurityItem(SecurityRequirement()
            .addList("Google Auth Token"))
}