package com.tinkoff.sirius.financial_tracker.filters

import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletRequestWrapper

/**
 * Class to change headers
 * Src: https://stackoverflow.com/questions/48240291/adding-custom-header-to-request-via-filter
 */
class MutableHttpServletRequest(request: HttpServletRequest?) : HttpServletRequestWrapper(request) {
    // holds custom header and value mapping
    private val customHeaders: MutableMap<String, String>
    fun putHeader(name: String, value: String) {
        customHeaders[name] = value
    }

    override fun getHeader(name: String): String? {
        // check the custom headers first
        val headerValue = customHeaders[name]
        return headerValue ?: (request as HttpServletRequest).getHeader(name)
        // else return from into the original wrapped object
    }

    override fun getHeaders(name: String): Enumeration<String> {
        // create a set of the custom header names
        val headerValue = customHeaders[name]
        if (headerValue != null) {
            val set: MutableSet<String> = HashSet()
            set.add(headerValue)
            return Collections.enumeration(set)
        }
        // else return from into the original wrapped object
        return (request as HttpServletRequest).getHeaders(name)
    }

    override fun getHeaderNames(): Enumeration<String> {
        // create a set of the custom header names
        val set: MutableSet<String> = HashSet(customHeaders.keys)

        // now add the headers from the wrapped request object
        val e = (request as HttpServletRequest).headerNames
        while (e.hasMoreElements()) {
            // add the names of the request headers into the list
            val n = e.nextElement()
            set.add(n)
        }
        // create an enumeration from the set and return
        return Collections.enumeration(set)
    }

    init {
        customHeaders = HashMap()
    }
}