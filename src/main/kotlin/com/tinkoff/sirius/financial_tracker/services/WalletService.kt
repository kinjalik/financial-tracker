package com.tinkoff.sirius.financial_tracker.services

import com.tinkoff.sirius.financial_tracker.converter.WalletConverter
import com.tinkoff.sirius.financial_tracker.dto.CurrencyDto
import com.tinkoff.sirius.financial_tracker.dto.WalletDto
import com.tinkoff.sirius.financial_tracker.dto.WalletStatDto
import com.tinkoff.sirius.financial_tracker.entities.Transaction
import com.tinkoff.sirius.financial_tracker.entities.Wallet
import com.tinkoff.sirius.financial_tracker.exceptions.ResourceNotFoundException
import com.tinkoff.sirius.financial_tracker.repositories.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import kotlin.math.exp

@Service
class WalletService(
    private val walletRepository: WalletRepository,
    private val walletConverter: WalletConverter,
    private val userService: UserService,
    private val transactionService: TransactionService,
    private val currencyService: CurrencyService
    ) {

    fun findWalletById(id : Int): WalletDto? {
        val income = transactionService.incomeTransactionAmountByWalletId(id).result
        val expenditure = transactionService.expenditureTransactionAmountByWalletId(id).result
        return walletConverter.convert(walletRepository.findWalletById(id), income, expenditure)
    }

    fun addWallet(walletDto: WalletDto, userEmail: String): WalletDto =
        addWallet(walletDto, userService.findOrCreateUserByEmail(userEmail).id!!)

    fun addWallet(walletDto: WalletDto, userId: Int): WalletDto {
        val currencyName = walletDto.currency.shortName
        val currency = currencyService.getCurrencyByShortName(currencyName)
            ?: throw ResourceNotFoundException("Provided currency does not exist")

        val wallet = walletConverter.convert(walletDto, userId, currency)

        val walletResult = walletRepository.save(wallet)
        return walletConverter.convert(walletResult, 0, 0)
    }

    fun findWalletsByUserEmail(userEmail: String): List<WalletDto> {
        val userId = userService.findOrCreateUserByEmail(userEmail).id!!
        return findWalletsByUserId(userId)
    }

    fun findWalletsByUserId(userId : Int) : List<WalletDto> {
        return walletRepository.findWalletsByUserId(userId).map {
            val walletId = it.id!!
            val income = getIncome(walletId)
            val expenditure = getExpenditure(walletId)
            walletConverter.convert(it, income, expenditure)
        }
    }

    fun getIncome(walletId : Int) = transactionService.incomeTransactionAmountByWalletId(walletId).result

    fun getExpenditure(walletId : Int) = transactionService.expenditureTransactionAmountByWalletId(walletId).result

    fun hideWallet(id: Int) : Wallet {
        val wallet = walletRepository.getById(id)
        wallet.isHidden = true
        return walletRepository.save(wallet)
    }

    fun showWallet(id: Int) : Wallet {
        val wallet = walletRepository.getById(id)
        wallet.isHidden = false
        return walletRepository.save(wallet)
    }

    fun deleteWalletById(id: Int) {
        transactionService.deleteTransactionByWalletId(id)
        walletRepository.deleteWalletById(id)
    }

    fun updateWalletById(id : Int, wallet: WalletDto): WalletDto {
        val newWallet = walletRepository.findWalletById(id)
        newWallet.name = wallet.name
        newWallet.balanceLimit = wallet.balanceLimit
        return walletConverter.convert(walletRepository.save(newWallet), getIncome(id), getExpenditure(id))
    }

    fun getWalletStatDto(userEmail: String, currencyDto: CurrencyDto): WalletStatDto {
        val userId = userService.findOrCreateUserByEmail(userEmail).id!!
        val walletList = walletRepository.findWalletsByUserId(userId)
        val income = walletList.sumOf { transactionService.incomeTransactionAmountByWalletId(it.id!!).result.toDouble() *
                                                    currencyService.getConvertValue(it.currency.shortName, "RUB", false) }
        val expenditure = walletList.sumOf { transactionService.expenditureTransactionAmountByWalletId(it.id!!).result.toDouble() *
                currencyService.getConvertValue(it.currency.shortName, "RUB", false) }
        return WalletStatDto(
            name="Common",
            currency=currencyDto,
            sum=(income - expenditure).toInt(),
            income=(income).toInt(),
            expenditure=(expenditure).toInt()
        )
    }
}