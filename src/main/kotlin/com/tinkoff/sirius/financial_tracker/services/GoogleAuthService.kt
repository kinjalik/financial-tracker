package com.tinkoff.sirius.financial_tracker.services

import com.google.api.client.auth.openidconnect.IdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.HttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.gson.GsonFactory
import com.tinkoff.sirius.financial_tracker.exceptions.AuthorizationException

import org.springframework.stereotype.Service
import java.security.GeneralSecurityException


@Service
class GoogleAuthService {
    private val transport: HttpTransport = NetHttpTransport()
    private val jsonFactory: JsonFactory = GsonFactory()


    fun validateAndRetrieveEmail(idTokenString: String): String {
        val payload = validate(idTokenString)
        // Get profile information from payload
        return payload.get("email") as String
    }

    /**
     * If [idTokenString] is a valid token returns [IdToken.Payload]
     * Else throws [AuthorizationException]
     */
    fun validate(idTokenString: String): IdToken.Payload {
        // TODO move to bean?
        val verifier = GoogleIdTokenVerifier.Builder(transport, jsonFactory)
            // TODO Specify the CLIENT_ID of the app that accesses the backend to check if it's our client
            .setAudience(null)
            .setIssuers(listOf("https://accounts.google.com", "accounts.google.com"))
            .build()

        var idToken: GoogleIdToken? = null
        try {
            idToken = verifier.verify(idTokenString)
        } catch(ex: IllegalArgumentException) { /* idToken will be null */ }

        if (idToken != null) {
//            println("user id ${idToken.payload.subject}")
            return idToken.payload
        } else {
            throw AuthorizationException("Invalid or expired Google auth token")
        }
    }
}