package com.tinkoff.sirius.financial_tracker.services

import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.tinkoff.sirius.financial_tracker.converter.CurrencyConverter
import com.tinkoff.sirius.financial_tracker.dto.CurrencyDto
import com.tinkoff.sirius.financial_tracker.entities.Currency
import com.tinkoff.sirius.financial_tracker.repositories.CurrencyRepository
import org.springframework.stereotype.Service
import java.net.URL

@Service
class CurrencyService(
    private val currencyRepository: CurrencyRepository,
    private val currencyConverter: CurrencyConverter
) {
    fun getCurrencyByShortName(shortName: String): Currency? =
        currencyRepository.getByShortName(shortName)

    fun getAllCurrencies(): List<CurrencyDto> =
        currencyRepository.findAll().map(currencyConverter::convert)

    /**
     * Returns existing currency with the shortname of [currency] or creates a new one
     */
    fun createIfNotExists(currency: Currency): Currency {
        return currencyRepository.getByShortName(currency.shortName)
            ?: currencyRepository.save(currency)
    }

    val strWrapper = "https://free.currconv.com/api/v7/convert?q=%s&compact=ultra&apiKey=2de7a749b5aa7c8925ef"

    fun getParse(str : String) = String.format(strWrapper, str)

    fun getConvertValue(input: String, output: String, shouldFallen : Boolean) : Double {
        val convertStr = "${input}_${output}"
        try {
            val token = getParse(convertStr)
            val json: JsonElement = JsonParser.parseString(URL(token).readText())
            return json.asJsonObject.get(convertStr).asDouble
        } catch (exception : Exception) {
            return if (input == "USD") {
                74.28
            } else if (input == "EUR") {
                87.28
            } else if (input == "RUB") {
                1.0
            } else {
                
                if (shouldFallen) {
                    throw exception
                } else {
                    return 1.0
                }
            }
        }
    }
}