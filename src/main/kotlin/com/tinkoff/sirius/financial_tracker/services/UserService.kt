package com.tinkoff.sirius.financial_tracker.services

import com.tinkoff.sirius.financial_tracker.entities.User
import com.tinkoff.sirius.financial_tracker.repositories.UserRepository
import org.springframework.stereotype.Service

@Service
class UserService(
    private val userRepository: UserRepository) {

    fun findUserById(id: Int): User? {
        return userRepository.findUserById(id)
    }

    fun findOrCreateUserByEmail(email: String): User {
        return userRepository.findUserByEmail(email)
            ?: userRepository.save(User(null, email))
    }
}