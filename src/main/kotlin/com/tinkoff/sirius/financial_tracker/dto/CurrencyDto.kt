package com.tinkoff.sirius.financial_tracker.dto

data class CurrencyDto(
    var shortName: String,
    var decimalDigits: Int?
)