package com.tinkoff.sirius.financial_tracker.dto

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDateTime

data class TransactionDto(
    var id: Int? = null,
    var walletId: Int,
    var categoryId: Int,
    var amount: Int,
    var date: String? = null
)
