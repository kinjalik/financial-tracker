package com.tinkoff.sirius.financial_tracker.dto

data class WalletStatDto (
    var name: String? = null,
    var currency: CurrencyDto,
    var sum: Int? = null,
    var income : Int? = null,
    var expenditure : Int? = null,
)