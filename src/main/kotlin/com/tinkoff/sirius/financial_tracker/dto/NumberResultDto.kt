package com.tinkoff.sirius.financial_tracker.dto

data class NumberResultDto(
    var result: Int
)