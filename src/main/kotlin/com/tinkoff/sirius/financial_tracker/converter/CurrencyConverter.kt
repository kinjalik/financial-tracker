package com.tinkoff.sirius.financial_tracker.converter

import com.tinkoff.sirius.financial_tracker.dto.CurrencyDto
import com.tinkoff.sirius.financial_tracker.entities.Currency
import org.springframework.stereotype.Component

@Component
class CurrencyConverter {

    fun convert(currency: Currency): CurrencyDto =
        CurrencyDto(shortName = currency.shortName, decimalDigits = currency.decimalDigits)


    fun convert(currency: CurrencyDto, id: Int, decimalDigits: Int): Currency =
        Currency(id = id, shortName = currency.shortName, decimalDigits = decimalDigits)

}