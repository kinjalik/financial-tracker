package com.tinkoff.sirius.financial_tracker.converter

import com.tinkoff.sirius.financial_tracker.dto.WalletDto
import com.tinkoff.sirius.financial_tracker.entities.Currency
import com.tinkoff.sirius.financial_tracker.entities.Wallet
import com.tinkoff.sirius.financial_tracker.services.TransactionService
import org.springframework.stereotype.Component
import kotlin.math.exp

@Component
class WalletConverter(
    private val currencyConverter: CurrencyConverter, private val transactionService: TransactionService
    ) {

    fun convert(wallet: WalletDto, userId: Int, currency: Currency): Wallet = Wallet(
        id = wallet.id,
        userId = userId,
        name = wallet.name,
        currency = currency,
        balanceLimit = wallet.balanceLimit,
        isHidden = wallet.isHidden
    )

    fun convert(wallet: Wallet, income: Int?, expenditure: Int?): WalletDto = WalletDto(
        id = wallet.id,
        name = wallet.name,
        currency = currencyConverter.convert(wallet.currency),
        balanceLimit = wallet.balanceLimit,
        income = income,
        expenditure = expenditure,
        isHidden = wallet.isHidden
    )
}