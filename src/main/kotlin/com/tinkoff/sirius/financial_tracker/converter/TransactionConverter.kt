package com.tinkoff.sirius.financial_tracker.converter

import com.tinkoff.sirius.financial_tracker.dto.TransactionDto
import com.tinkoff.sirius.financial_tracker.entities.Transaction
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Component
class TransactionConverter {
    private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
    fun convert(tx: Transaction): TransactionDto = TransactionDto(
        id = tx.id,
        walletId = tx.walletId,
        categoryId = tx.categoryId,
        amount = tx.amount,
        date = formatter.format(tx.date)
    )

    fun convert(tx: TransactionDto) = Transaction(
        id = tx.id,
        walletId = tx.walletId,
        categoryId = tx.categoryId,
        amount = tx.amount,
        // ToDo: Read about @CreationTimestamp
        date = if (tx.date == null)
            LocalDateTime.now()
        else
            LocalDateTime.parse(tx.date, formatter)
    )
}