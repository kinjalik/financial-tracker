package com.tinkoff.sirius.financial_tracker.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.FORBIDDEN)
class SecurityViolationException(msg: String) : Exception(msg)