package com.tinkoff.sirius.financial_tracker.converter

import com.tinkoff.sirius.financial_tracker.dto.CurrencyDto
import com.tinkoff.sirius.financial_tracker.entities.Currency
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

class CurrencyConverterTest {
    val entity = Currency(
        id = 322,
        shortName = "DGE",
        decimalDigits = 28
    )
    val dto = CurrencyDto(
        shortName = "DGE",
        decimalDigits = 28
    )

    @Test
    fun `DTO to Entity`() {
        val converter = CurrencyConverter()

        val result = converter.convert(dto, 322, 28)

        assertEquals(entity, result)
    }

    @Test
    fun `Entity to DTO`() {
        val converter = CurrencyConverter()

        val result = converter.convert(entity)

        assertEquals(dto, result)
    }
}