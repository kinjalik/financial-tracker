package com.tinkoff.sirius.financial_tracker.services

import com.tinkoff.sirius.financial_tracker.converter.CurrencyConverter
import com.tinkoff.sirius.financial_tracker.dto.CurrencyDto
import com.tinkoff.sirius.financial_tracker.entities.Currency
import com.tinkoff.sirius.financial_tracker.repositories.CurrencyRepository
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.mockito.Mockito


class CurrencyServiceTests(
) {
    // Looks like the most useless test i've ever write
    @Test
    fun `get all currencies`() {
        val currencyConverter = Mockito.mock(CurrencyConverter::class.java)
        val currencyRepository = Mockito.mock(CurrencyRepository::class.java)
        val testCurrencies = listOf(
            Currency(1, "RUB", 2),
            Currency(2, "BTC", 8),
            Currency(3, "ETH", 18)
        )
        val testCurrencyDtos = listOf(
            CurrencyDto("RUB", 2),
            CurrencyDto("BTC", 8),
            CurrencyDto("ETH", 18)
        )
        Mockito.`when`(currencyRepository.findAll()).thenReturn(testCurrencies)
        testCurrencies.indices.forEach {
            Mockito.`when`(currencyConverter.convert(testCurrencies[it])).thenReturn(testCurrencyDtos[it])
        }

        val currencyService = CurrencyService(currencyRepository, currencyConverter)
        val result: List<CurrencyDto> = currencyService.getAllCurrencies()

        assertEquals(result, testCurrencyDtos)
    }
}