package com.tinkoff.sirius.financial_tracker.services

import com.tinkoff.sirius.financial_tracker.entities.TransactionCategory
import com.tinkoff.sirius.financial_tracker.entities.User
import com.tinkoff.sirius.financial_tracker.repositories.TransactionCategoryRepository
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.mockito.Mockito

internal class TransactionCategoryServiceTest {
    private val categories = listOf(
        TransactionCategory(id = 1, name="Зарплата", isIncome = true, iconColor = 22523522, iconLink = "icons/42.svg"),
        TransactionCategory(id = 2, name="Общественный транспорт", isIncome = false, iconColor = 9549, iconLink = "icons/21.svg")
    )

    @Test
    fun getAllTxCategories() {
        val repository = Mockito.mock(TransactionCategoryRepository::class.java)
        val userService = Mockito.mock(UserService::class.java)
        Mockito.`when`(repository.findAll().toList()).thenReturn(categories)

        val service = TransactionCategoryService(repository, userService)

        val result = service.getAllTxCategories()

        assertEquals(result, categories)
    }

    @Test
    fun `create category which already exists`() {
        val repository = Mockito.mock(TransactionCategoryRepository::class.java)
        val userService = Mockito.mock(UserService::class.java)
        Mockito.`when`(repository.findByName("Зарплата")).thenReturn(categories[0])

        val service = TransactionCategoryService(repository, userService)

        val result = service.createIfNotExists(
            TransactionCategory(name = "Зарплата", isIncome = false), null
        )

        assertEquals(result, categories[0])
    }

    @Test
    fun `create category which doesn't exist`() {
        val testCategoryInput = TransactionCategory(
            name = "Продукты",
            isIncome = false,
            iconColor = 42942,
            iconLink = "icons/54.svg"
        )
        val testUserEmail = "category@test.com"
        val testUserId = 228
        val testCategoryOutput = TransactionCategory(
            id = 3,
            name = testCategoryInput.name,
            isIncome =  testCategoryInput.isIncome,
            iconColor = testCategoryInput.iconColor,
            iconLink = testCategoryInput.iconLink
        )

        val repository = Mockito.mock(TransactionCategoryRepository::class.java)
        val userService = Mockito.mock(UserService::class.java)
        Mockito.`when`(repository.findByName("Продукты")).thenReturn(null)
        Mockito.`when`(repository.save(testCategoryInput)).thenReturn(testCategoryOutput)
        Mockito.`when`(userService.findOrCreateUserByEmail(testUserEmail)).thenReturn(User(
            testUserId, testUserEmail
        ))

        val service = TransactionCategoryService(repository, userService)
        val result = service.createIfNotExists(testCategoryInput, testUserEmail)

        assertEquals(result, testCategoryOutput)
    }

    @Test
    fun `get transaction categories for user by email`() {
        val email = "unit@test.com"
        val testUserId = 1

        val testUser = User(
            id = testUserId,
            email = email
        )
        val categoryForAll = TransactionCategory(
            id = 0,
            name = "Доход",
            isIncome = true,
            iconColor = 228322,
            iconLink = "SomeLink",
            userId = null
        )
        val categoryForUser = TransactionCategory(
            id = 1,
            name = "Долг",
            isIncome = false,
            iconColor = 1337420,
            iconLink = "SomeLink",
            userId = testUserId
        )

        val userService = Mockito.mock(UserService::class.java)
        val repository = Mockito.mock(TransactionCategoryRepository::class.java)
        Mockito.`when`(userService.findOrCreateUserByEmail(email)).thenReturn(testUser)
        Mockito.`when`(repository.getAllByUserIdOrUserId(testUserId)).thenReturn(listOf(categoryForAll, categoryForUser))

        val service = TransactionCategoryService(repository, userService)
        val result = service.getTxCategoriesForUserByEmail(email)

        assertEquals(listOf(categoryForAll, categoryForUser), result)

    }
}