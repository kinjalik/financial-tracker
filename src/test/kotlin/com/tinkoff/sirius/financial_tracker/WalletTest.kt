package com.tinkoff.sirius.financial_tracker

import com.tinkoff.sirius.financial_tracker.controller.TransactionController
import com.tinkoff.sirius.financial_tracker.controller.WalletController
import com.tinkoff.sirius.financial_tracker.converter.CurrencyConverter
import com.tinkoff.sirius.financial_tracker.dto.TransactionDto
import com.tinkoff.sirius.financial_tracker.dto.WalletDto
import com.tinkoff.sirius.financial_tracker.entities.Currency
import com.tinkoff.sirius.financial_tracker.services.CurrencyService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDateTime
import kotlin.math.min
import kotlin.random.Random

@SpringBootTest
internal class WalletTest {

    @Autowired
    lateinit var walletController: WalletController

    @Autowired
    lateinit var transactionController : TransactionController

    @Autowired
    lateinit var currencyService: CurrencyService

    @Autowired
    lateinit var currencyConverter: CurrencyConverter


    fun initWallet(walletExpected : WalletDto, walletActual : WalletDto) {
        walletExpected.id = walletActual.id
        walletExpected.income = 0
        walletExpected.expenditure = 0
    }

    fun createTransaction(value : Int, walletId : Int, email: String) {
        val trans = TransactionDto(
            walletId = walletId,
            categoryId = 0,
            amount = value
        )
        transactionController.addTransaction(trans, email)
    }

    fun initWallet(walletExpected: WalletDto, email : String) {
        walletController.addWallet(walletExpected, email)
        initWallet(walletExpected, getWalletActual(email))
        assertEquals(walletExpected, getWalletActual(email))
    }

    @Test
    fun addWalletByUserEmailTest() {
        val email = "mkr@mail.ru"
        val walletExpected = WalletDto(
            name = "misha",
            currency = currencyConverter.convert(currencyService.getCurrencyByShortName("RUB")!!),
        )
        initWallet(walletExpected, email)
    }

    fun getWalletActual(email : String) = walletController.getWallets(email)[0]

    @Test
    fun changeWalletTest() {
        val email = "mki@mail.ru"
        val currency = currencyConverter.convert(currencyService.getCurrencyByShortName("RUB")!!)
        val walletExpected = WalletDto(
            name = "cat",
            currency = currency,
        )
        initWallet(walletExpected, email)

        createTransaction(1999, getWalletActual(email).id!!, email)
        walletExpected.income = 1999
        assertEquals(walletExpected, getWalletActual(email))

        createTransaction(1000, getWalletActual(email).id!!, email)
        walletExpected.income = walletExpected.income!! + 1000
        assertEquals(walletExpected, getWalletActual(email))

        createTransaction(-1000, getWalletActual(email).id!!, email)
        walletExpected.expenditure = walletExpected.expenditure!! + 1000
        assertEquals(walletExpected, getWalletActual(email))

        for (i in 0..100) {
            val value = Random.nextInt() / 1000
            createTransaction(value, getWalletActual(email).id!!, email)
            if (value > 0) {
                walletExpected.income = walletExpected.income!! + value
            } else {
                walletExpected.expenditure = walletExpected.expenditure!! - value
            }
            assertEquals(walletExpected, getWalletActual(email))
        }
    }

    @Test
    fun transactionsWalletTest() {
        val email = "udski@gmail.ru"
        val currency = currencyConverter.convert(currencyService.getCurrencyByShortName("RUB")!!)
        val walletExpected = WalletDto(
            name = "koko",
            currency = currency
        )
        initWallet(walletExpected, email)

        createTransaction(423, getWalletActual(email).id!!, email)
        walletExpected.income = 423

        createTransaction(-32, getWalletActual(email).id!!, email)
        walletExpected.expenditure = 32

        assertEquals(walletExpected, getWalletActual(email))
        val transactions = walletController.getTransactionsByWalletId(getWalletActual(email).id!!, null, 10)
        assertEquals(2, transactions.size)

        val actualIncome = transactions.sumOf { if (it.amount > 0) it.amount else 0 }
        val actualExpenditure = transactions.sumOf { if (it.amount < 0) -it.amount else 0 }

        assertEquals(walletExpected.income, actualIncome)
        assertEquals(walletExpected.id!!, transactions[0].walletId)
        assertEquals(walletExpected.id!!, transactions[1].walletId)
        assertEquals(walletExpected.expenditure, actualExpenditure)
        var income = walletExpected.income!!
        var expenditure = walletExpected.expenditure!!

        for (i in 0..100) {
            val value = Random.nextInt() / 1000
            createTransaction(value, getWalletActual(email).id!!, email)
            if (value > 0) {
                income += value
            } else {
                expenditure -= value
            }
        }
        val idActual = getWalletActual(email).id!!
        assertEquals(income, walletController.getIncomeIntegerPart(idActual).result)
        assertEquals(expenditure, walletController.getExpenditureIntegerPart(idActual).result)
        assertEquals(income - expenditure, walletController.sumTransactionsAmount(idActual).result)

    }

    @Test
    fun getTransactionsWithPagination() {
        val email = "pagination@test.xxx"
        val currency = currencyConverter.convert(currencyService.createIfNotExists(Currency(
            shortName = "ETH",
            decimalDigits = 18
        )))
        val walletExpected = WalletDto(
            name = "Test Wallet",
            currency = currency
        )
        var transactionAmounts = listOf(20, 30, 40, -50, -100, 1000, -20, 10, -30, 15, 5)
        val pageSize = 3
        val pageCount = transactionAmounts.size / pageSize + (if (transactionAmounts.size % pageSize > 0) 1 else 0)

        initWallet(walletExpected, email)
        val walletId = getWalletActual(email).id!!
        transactionAmounts.forEach { createTransaction(it, walletId,email) }


        // Test: number of pages
        var actualCurrentPage: List<TransactionDto>? = null
        var actualPageCount: Int = 0
        var lastTxId: Int? = null
        transactionAmounts = transactionAmounts.reversed()
        do {
            val startTxId = actualPageCount * 3
            val endTxId = min(actualPageCount * 3 + pageSize - 1, transactionAmounts.size)
            val currentPage = transactionAmounts.slice(startTxId until endTxId)

            actualCurrentPage = walletController.getTransactionsByWalletId(walletId, lastTxId, pageSize)
            actualPageCount++
            lastTxId = actualCurrentPage.last().id

            for (i in currentPage.indices) {
                assertEquals(currentPage[i], actualCurrentPage[i].amount)
            }
        } while (actualCurrentPage!!.size == pageSize)

        assertEquals(pageCount, actualPageCount)

    }

}